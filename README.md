# GRUB Multiboot

## Table of contents



## Foreword

The goal of this project is to use GRUB to start either Ubuntu or OpenCore/macOS

## Hints



- ```bash
  $ sudo parted -l
  
  --snip--
  
  Model: Samsung SSD 970 EVO Plus 500GB (nvme)
  Disk /dev/nvme0n1: 500GB
  Sector size (logical/physical): 512B/512B
  Partition Table: gpt
  Disk Flags: 
  
  Number  Start   End    Size   File system  Name                  Flags
   1      20,5kB  210MB  210MB  fat32        EFI System Partition  boot, esp
   2      210MB   500GB  500GB
  
  ```

- ```bash
  $ lsblk -f
  
  --snip--
  
  NAME        FSTYPE   LABEL             UUID                                 FSAVAIL FSUSE% MOUNTPOINT
  
  nvme0n1                                                                                    
  ├─nvme0n1p1 vfat     EFI               XXXX-XXXX                                           
  └─nvme0n1p2 apfs                       XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX  
  ```

- 



## Config

Add the following to `/etc/grub.d/40_custom`:

```
menuentry "MacOS" {
  # Search the root device for Mac OS X's loader.
  search --set=root --fs-uuid 67E3-17ED
  search --file --no-floppy --set=root /EFI/BOOT/BOOTx64.efi
  # chainload the loader, pass parameters like -v directly
  chainloader (${root})/EFI/BOOT/BOOTx64.efi -v
}
```

To apply changes run `sudo update-grub`

Documentation: https://help.ubuntu.com/community/Grub2

## Credits



## Table of refernces

1. [Multi-boot manual config](https://www.gnu.org/software/grub/manual/grub/html_node/Multi_002dboot-manual-config.html)
2. [Installing GRUB using grub-install](https://www.gnu.org/software/grub/manual/grub/html_node/Installing-GRUB-using-grub_002dinstall.html)
3. [Linux Hard Disk Format Command](https://www.cyberciti.biz/faq/linux-disk-format/)
4. [Add grub menu for OS X](https://askubuntu.com/questions/765254/add-grub-menu-for-os-x)
5. [OpenCore](https://github.com/acidanthera/OpenCorePkg/releases)
6. [os-prober](https://joeyh.name/code/os-prober/)
7. [How to use os-prober to find MS-Windows boot data?](https://unix.stackexchange.com/questions/350079/how-to-use-os-prober-to-find-ms-windows-boot-data)

